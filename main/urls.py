from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^login/', views.login, name='login'),
    url(r'^index/', views.index, name='index'),
    url(r'^rest/', include('rest.urls')),
    url(r'^$', views.index, name='index')
]
