class Control:
    USER = 'user'
    PASS = 'pass'
    STATUS = 'status'
    DEVICE = 'device'
    USERTYPE = 'usertype'


class RegChild:
    NAME = 'name'
    USERMASTER = 'usermaster'
    PASSMASTER = 'passmaster'
    USERCHILD = 'userchild'
    CHILDPASS = 'childpass'
    TIMEOFEXP = 'time'
    DEVICES = 'devices'


class RegUser:
    USER = 'user'
    PASS = 'pass'
    NAME = 'name'
    EMAIL = 'email'
    PHONE = 'phone'
    DOR = 'dor'


class Login:
    USER = 'user'
    PASS = 'pass'


class DeviceConst:
    DEVICE_ID = 'devid'
    DEVICE_NAME = 'devname'
    PIN_MAP = 'pinmap'
    AUTH_KEY = 'auth'


class UserType:
    MASTER = 'master'
    CHILD = 'child'


# New Requests Classes
class Authenticate:
    USERNAME = 'user'
    PASSWORD = 'pass'


class AddUser:
    EMAIL = 'email'
    PASSWORD = 'pass'
    CHILDUSER = 'childuser'
    LOCKID = 'lockid'
    STARTTIME = 'starttime'
    EXPTIME = 'exptime'


class HistoryConst:
    USER = 'user'
    PASS = 'pass'
    STARTDATE = 'startdate'
    ENDDATE = 'enddate'


class Operation:
    EMAIL = 'email'
    PASSWORD = 'pass'
    STATUS = 'status'
    DEVICE_ID = 'device'


class ModeChange:
    EMAIL = 'email'
    PASSWORD = 'pass'


class Status:
    EMAIL = 'email'
    PASSWORD = 'pass'


class BlockUser:
    EMAIL = 'email'
    PASSWORD = 'pass'
    BLOCK_USER = 'user'


class AddLock:
    EMAIL = 'email'
    PASSWORD = 'pass'
    LOCKID = 'lockid'
    FOR_USER = 'foruser'
