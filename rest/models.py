from django.db import models
import json
from .constants import UserType


# Create your models here.

class User(models.Model):
    name = models.CharField(max_length=50)
    dob = models.CharField(max_length=10)
    address = models.CharField(max_length=200)
    email = models.CharField(max_length=80, primary_key=True, unique=True)
    phone = models.CharField(max_length=13)
    password = models.CharField(max_length=100)
    dor = models.CharField(max_length=20)
    child_of=models.TextField(default='[]')
    passresettoken=models.TextField(default='')
    notify_key=models.TextField(default='')

    def __str__(self):
        return self.email


def authenticate(username, password):
    # models.get

    users = User.objects.filter(email=username, password=password)
    if users.exists():
        return True
    else:
        return False


class Device(models.Model):
    device_id = models.CharField(max_length=13, primary_key=True, unique=True)
    device_name = models.CharField(max_length=20, null=False)
    pin_map = models.CharField(max_length=20, null=False)
    status = models.CharField(max_length=20, null=False)
    masteruser = models.ForeignKey(User, on_delete=models.CASCADE)
    childuser = models.TextField(default='[]')
    mode = models.TextField(max_length=2, default='1')  # 0-> offline 1-> online
    module_status = models.TextField(max_length=20, null=False)
    module_mode = models.CharField(max_length=2,default='0')
    battery_status = models.TextField(max_length=3,default='0')
    macid = models.TextField(max_length=20,default='mm:mm:mm:ss:ss:ss')
    
    def __str__(self):
        return self.device_id + "," + self.device_name


def checkAndGetDevice(device_id, usertype, user):
    try:
        if usertype == UserType.MASTER:
            device = Device.objects.filter(device_id=device_id, masteruser=user)
            if device.exists:
                # return device.device_name + "@" + device.device_id
                return True

        elif usertype == UserType.CHILD:
            device_child = Device.objects.filter(device_id=device_id)
            if device_child.exists:
                children = json.loads(device_child.childuser)
                authentic = False
                for child in children:
                    if child == user:
                        authentic = True
                        break

            else:
                return False

    except IndexError:  # return 'Sorry no device is registered with device id ' + device_id
        return False


class Home(models.Model):
    home_id = models.CharField(max_length=20, primary_key=True, unique=True)
    master_username = models.ForeignKey(User, models.CASCADE)
    devices = models.TextField()
    cur_status = models.TextField()

    def __str__(self):
        return self.home_id + " @ " + self.master_username


class History(models.Model):
    device = models.ForeignKey(Device, on_delete=models.CASCADE)
    status = models.CharField(max_length=20)
    username = models.ForeignKey(User, on_delete=models.CASCADE)
    mode = models.CharField(max_length=10)  # child or master
    dateandtime=models.TextField()

    def __str__(self):
        return str(self.device) + "History"
