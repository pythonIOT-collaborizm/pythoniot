from django.conf.urls import url

from . import views

app_name = 'rest'

urlpatterns = [

    url(r'^mobv01/rest$', views.rest, name='rest'),  # Doc Message
    url(r'^mobv01/control$', views.control, name='control'),  # Action Controls the device status
    url(r'^mobv01/showparams$', views.showparams, name='params'),  # Shows the parameters required for the endpoint

    url(r'^mobv01/history$', views.history, name='history'),  # Shows the history of the user logged in
    url(r'^mobv01/getdevices$', views.getdevices, name='getdevices'),  # Show the devices of a user logged in
    url(r'^mobv01/adduser$', views.addUser, name='addusers'),  #    adds child user to the database for a device
    url(r'^mobv01/updateprofile$', views.updateProfile, name='updateprofile'), #updates the profile of a user
    url(r'^mobv01/regdevice$', views.registerdevice, name='regdevice'),  # Registers a device to the user end 
    url(r'^mobv01/auth$', views.login, name='auth'),  # Logs in a user
    url(r'^mobv01/reguser$', views.registerUser, name='reguser'),  # Registers a user to the device 
    url(r'^mobv01/deventry$', views.deviceEntry, name='deventry'),  # At the manufacturer end registers a device
    url(r'^mobv01/removechild$', views.removeUser, name='removeuser'),  # Remove a child user from the device userslist
    url(r'^mobv01/togglemode$', views.toggleMode, name='removeuser'),  # Changes the mode of operation i.e., from online to offline or vice versa
    url(r'^mobv01/ack$', views.acknowledge, name='acknowledgement'),  # Acknowledgement to the user about successful operation
    url(r'^mobv01/forgot$', views.passwordReset, name="passwordReset"),  # Action password reset
    url(r'^mobv01/sendmail$', views.sendmail, name="sendmail"),  # Action password reset sends otp via email to reset password
    url(r'^mobv01/changepassword$', views.changePassword, name="changepassword"),  # Action change the password from profile section
    url(r'^mobv01/forgetreset$', views.changePasswordAfterForgot, name="changePasswordAfterForgot"), #Change password after forgot
    url(r'^mobv01/registertoken', views.register_token, name="registertoken"), # Registers a firebase token for the user to receive push notification

    url(r'^mobv01/notify$', views.notifyTest, name="notifyTest"),
    # For espmodule
    url(r'^espv01/getstatus$', views.getStatusESP, name='epscontrol'),  # Action

]

# For esp
'''
    readStatus of espmodule to respond as similar or dissimilar value
    parameters : <cipher>$0/1(online offline)$mode$data ->

'''
