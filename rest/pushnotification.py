import requests
import json
from .models import User


def send_notification(tokens, message,title):
    url = 'https://fcm.googleapis.com/fcm/send'
    fields = {'registration_ids': tokens,
              'notification': {
                  'title':title,
                  'body':message
              }}
    headers = {
        'Authorization':'key = AAAAJs14tF0:APA91bHG7o1J6iAY944a_vZXAeEKEKwXIT5dhi9zVEnpfgajhPpLTLlWwY1DoChFXAWU-qcl6Avpmu8ZI-H0-XD8vhtNN3-qzYsJC-csr07PTLq5fdvxxDsqmO0aCTwEiZXnhJK92Th7',
        'Content-Type':'application/json'}
    response = requests.post(url=url, headers=headers, data=json.dumps(fields))

    return response.text
