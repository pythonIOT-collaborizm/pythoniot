import datetime

from django.core.mail import BadHeaderError
from django.core.mail import EmailMessage
from django.db import IntegrityError
from django.http import HttpResponse
from django.utils.datastructures import MultiValueDictKeyError
from django.views.decorators.csrf import csrf_exempt

from .constants import *
from .models import *
from .models import Device
from .pushnotification import *


# Create your views here.

def rest(request):
    message = '''

    <html>

    <head><title>Rest API Docs</title></head>

    <body>

        <br/> <h2>Welcome to the Python IoT Rest API Docs for Mobile</h2><br/>

        <p>Please refer the following table for the url and its response <br/>

        <b>Note that the base url for this rest API is : http://domain/rest/mobv01 </b>

        <br/>

        "domain" refers to the domain you are using to access this page<br/>

        Example: www.example.com. Replace the domain with your own domain.

        <br/>



        </p>

        <table border="2px">

	<thead>

		<th>URL</th>

		<th>Parameter</th>

		<th>Response</th>

	</thead>

	<tbody>

		<tr>

			<td>domain/rest/mobv01/rest</td>

			<td>-</td>

			<td>This Documentation page</td>

		</tr>

		<tr>

			<td>domain/rest/mobv01/control</td>

			<td>user,pass,status,device,usertype</td>

			<td>JSON Object having module_status,device_id,battery_status (if success) else relevant error message for diagnosis</td>

		</tr>
		<tr>

			<td>domain/rest/mobv01/history</td>

			<td>user,pass,devid</td>

			<td>JSON Object with device, status, mode,user (if success) else relevant error message</td>

		</tr>

		<tr>

			<td>domain/rest/mobv01/getdevices</td>

			<td>user, pass</td>

			<td>JSON Object of all the devices for the given on success user name else relevant error diagnostic message</td>

		</tr>

		<tr>

			<td>domain/rest/mobv01/adduser</td>

			<td>user,pass,devid,starttime,endtime,child</td>

			<td>$success$ with json object else error with Diagnostic message</td>

		</tr>

		<tr>

			<td>domain/rest/mobv01/regdevice</td>

			<td>devid,devname,auth,user,pass</td>

			<td>$success$ with json object else error with diagnostic message</td>

		</tr>

		<tr>

			<td>domain/rest/mobv01/auth</td>

			<td>user, pass</td>

			<td>JSON Object with all the relevant user data from the database</td>

		</tr>

		<tr>

			<td>domain/rest/mobv01/reguser</td>

			<td>name, dor, email, pass, phone</td>

			<td>$success$ on successful registration of a user else error with dianostic message</td>

		</tr>

		<tr>

			<td>domain/rest/mobv01/deventry</td>

			<td>devid, devname, pinmap, auth</td>

			<td>Success message on successful operation else error with diagnostic message</td>

		</tr>

		<tr>

			<td>domain/rest/mobv01/removechild</td>

			<td>user,pass,devid,child</td>

			<td>$success$ on successful removal of a child user from the device table else error with diagnostic message</td>

		</tr>

		<tr>

			<td>domain/rest/mobv01/togglemode</td>

			<td>user,pass,devid,usertype</td>

			<td>Changes the mode of the particular device with the specified device id and returns $success$ on successful operation else error with diagnostic message</td>

		</tr>

		<tr>

			<td>domain/rest/mobv01/ack</td>

			<td>user,pass,usertype,devid</td>

			<td>JSON Object to acknowledge a successful operation else error with diagnostic message</td>

		</tr>

		<tr>

			<td>domain/rest/espv01/getstatus</td>

			<td>data (with a predefined format)</td>

			<td>data with predefined format for the espmodule on successful fetch else error with dianostic message</td>

		</tr>

	</tbody>

</table>

        <footer>

        </footer>

    </body>

    </html>

            ''';

    return HttpResponse(message)


def showparams(request):
    try:

        keyword = request.GET['uri'];

        message = '''<table border="1px">

                        <thead>

                            <th>Url</th>

                                <th>Parameters</th>

                                <th>Response</th>

                            </thead>

                            `<tbody>

                                <tr>

                                    <td>http://domain/rest/mobv01/'''

        urls = {'rest': 'N/A', 'control': 'user,pass,status,device,usertype',

                'showparams': 'uri=control/history/regchild/regmaster',

                'history': 'user,pass', 'regchild': 'usermaster,passmaster,name,userchild,childpass,time,devices',

                'regmaster': 'user,pass,name,email,phone,dor', 'getdevices': 'user,pass'}

        key = urls[keyword]

        return HttpResponse(message + urls[keyword] + '''</td>

                                        <td>user,pass,status</td>

                                        <td>success or failed Depending upon the successful execution of query</td>

                                    </tr>

                                </tbody>

                            </table>
                            ''')



    except (MultiValueDictKeyError):

        return HttpResponse('{"req_status":"failed","message":"Please provide a uri parameter in your get request"}')

    except (KeyError):

        return HttpResponse('{"req_status":"failed","message":"Sorry the request uri parameter doesn\'t exist"}')


def deviceEntry(request):
    try:

        dev_id = str(request.GET["devid"])

        dev_name = request.GET["devname"]

        dev_pinmap = request.GET["pinmap"]

        userauth = request.GET["auth"]

        macid = request.GET["macid"]

        if userauth == 'epsumlabs_hash':

            user = User.objects.filter(email="admin@epsumlabs.com", password="epsumlabs")[0]

            device = Device(device_id=dev_id, device_name=dev_name, pin_map=dev_pinmap, status="NEW",

                            masteruser=user,macid=macid)

            device.save(force_insert=True)

            return HttpResponse('{"req_status":"success","message":"Successfully inserted your data into database"}')

        else:

            return HttpResponse('{"req_status":"failed","message":"Authentication error"}')

    except MultiValueDictKeyError:

        return HttpResponse('{"req_status":"failed","message":"Missed out some paramenters.. See docs for More info"}')

    except IndexError:

        return HttpResponse('{"req_status":"failed","message":"Sorry admin isn\'t registered Yet"}')

    except IntegrityError:

        return HttpResponse('{"req_status":"failed","message":"The device with the same device id is already registered"}')


def toggleMode(request):
    try:

        username = request.GET['user']

        password = request.GET['pass']

        deviceid = request.GET['devid']

        usertype = request.GET['usertype']

        mode_ = request.GET['mode']

        if authenticate(username=username, password=password):

            if usertype == UserType.MASTER and is_device_exists_for_master_user(username=username, device_id=deviceid):

                device = Device.objects.filter(device_id=deviceid,masteruser=username).update(mode=str(mode_))
                return HttpResponse('{"req_status":"success","current_mode":"' + str(mode_)+'"}')
                # device = Device.objects.filter(device_id=deviceid, masteruser=username)[0]
                #
                # if device.mode == '0':
                #
                #     device = Device.objects.filter(device_id=deviceid, masteruser=username).update(mode='1')
                #
                #     return HttpResponse('{"req_status":"success","current_mode":"' + str('1')+'"}')
                #
                # elif device.mode == '1':
                #
                #     device = Device.objects.filter(device_id=deviceid, masteruser=username).update(mode='0')
                #
                #     return HttpResponse('{"req_status":"success","current_mode":"' + str('0')+'"}')

            elif usertype == UserType.CHILD and is_device_exists_for_child_user(childuser=username, device_id=deviceid):

                device = Device.objects.filter(device_id=deviceid).update(mode=str(mode_))
                return HttpResponse('{"req_status":"success","current_mode":"' + str(mode_)+'"}')

                # device = Device.objects.filter(device_id=deviceid)[0]
                #
                #
                # if device.mode == '0':
                #
                #     device = Device.objects.filter(device_id=deviceid).update(mode='1')
                #
                #     return HttpResponse('{"req_status":"success","current_mode":"' + str('1')+'"}')
                #
                # elif device.mode == '1':
                #
                #     device = Device.objects.filter(device_id=deviceid).update(mode='0')
                #
                #     return HttpResponse('{"req_status":"success","current_mode":"' + str('0')+'"}')

        else:

            return HttpResponse('{"req_status":"failed","message":"Authentication Error"}')

    except MultiValueDictKeyError:

        return HttpResponse('"req_status":"failed","message":"Please provide all the required parameters to toggle mode"')


def is_device_exists(device_id):
    d = Device.objects.filter(device_id=device_id)

    if d.exists():

        return True

    else:

        return False


def is_device_exists_for_master_user(username, device_id):
    d = Device.objects.filter(device_id=device_id, masteruser=username)

    if d.exists():

        return True

    else:

        return False


def is_device_exists_for_child_user(childuser, device_id):
    d = Device.objects.filter(device_id=device_id)

    if d.exists():

        device = d[0]

        device_children = json.loads(device.childuser)

        flag = False

        for child in device_children:

            if child['user'] == childuser:

                flag = True

                break

            else:

                flag = False

        return flag

    else:

        return False


def acknowledge(request):
    try:

        username = str(request.GET['user'])

        password = str(request.GET['pass'])

        usertype = str(request.GET['usertype'])

        device_id = str(request.GET['devid'])

        auth = authenticate(username=username, password=password)

        if auth and usertype == UserType.MASTER:

            device = Device.objects.filter(device_id=device_id, masteruser=username)

            if device.exists():

                device_j = {"req_status":"success",'device_id': device[0].device_id, 'module_status': device[0].module_status,'set_mode':device[0].mode,'module_mode':device[0].module_mode}

                return HttpResponse(json.dumps(device_j))

            else:

                HttpResponse('{"req_status":"failed","message":"Device not found"}')

        elif auth and usertype == UserType.CHILD:

            device = Device.objects.filter(device_id=device_id)

            if device.exists():

                childuser = json.loads(device[0].childuser)

                flag = False

                for c in childuser:

                    if c['user'] == username:

                        flag = True

                        break

                    else:

                        flag = False

                if flag:
                    response = {"req_status":"success",'device_id': device[0].device_id, 'module_status': device[0].module_status,'set_mode':device[0].mode,'module_mode':device[0].module_mode}
                    response = json.dumps(response)
                    return HttpResponse(response)
                else:

                    return HttpResponse('{"req_status":"failed","message":"User does not exist"}')

            else:

                return HttpResponse('{"req_status":"failed","message":"Device doesnot exists"}')

    except MultiValueDictKeyError:

        return HttpResponse('{"status":"failed","message":"Please provide all the required parameters"}')

    except IndexError:

        return HttpResponse('{"status":"failed","message":"Device doesnot exist"}')


def control(request):
    try:

        username = str(request.GET['user'])

        password = str(request.GET['pass'])

        status = str(request.GET['status'])

        device = str(request.GET['device'])

        usertype = str(request.GET['usertype'])

        auth = (authenticate(username=username, password=password))
        from time import gmtime, strftime
        time_now = strftime("%d/%m/%Y-%H:%M:%S", gmtime())
        if auth and usertype == UserType.MASTER:

            device_res = Device.objects.filter(device_id=device, masteruser=username).update(status=status)

            dev_his = Device.objects.filter(device_id=device, masteruser=username)[0]

            user = User.objects.filter(email=username, password=password)[0]

            history_obj = History(device=dev_his, username=user, mode=usertype, status=status, dateandtime=time_now)

            history_obj.save(force_insert=True)

            if device_res == 1:

                dev_obj = Device.objects.filter(device_id=device, masteruser=username)[0]

                dev_obj = {"req_status":"success","device_status": dev_obj.status, "device_id": dev_obj.device_id,

                           "battery_status": dev_obj.battery_status,"op_status":str(device_res)}

                dev_obj = json.dumps(dev_obj)

                return HttpResponse(dev_obj)

            else:

                return HttpResponse('{"req_status":"failed","message":"Status not updated"}')

        elif auth and usertype == UserType.CHILD:

            device = Device.objects.filter(device_id=device)

            if device.exists():

                device = device[0]

                user = json.loads(device.childuser)

                flag = False

                for u in user:

                    if u['user'] == username:
                        date_limit = u['endtime']
                        if date_limit == '00/00/0000:00-00-00':
                            flag = True
                        else :
                            date_limit = date_limit.split('/')
                            date_limit[2] = date_limit[2][:4]
                            date_time_obj = datetime.date(int(str(date_limit[2])), int(str(date_limit[1])),
                                                      int(str(date_limit[0])))
                            date_now = datetime.datetime.today()
                            if date_time_obj < date_now :
                                return HttpResponse('{"req_status":"failed","message":"Date limit is crossed"}')

                            flag = True
                            user_history = u['user']
                            break

                    else:

                        flag = False

                if flag:

                    device_child = Device.objects.filter(device_id=device.device_id).update(status=status)
                    device_history = Device.objects.filter(device_id=device.device_id)[0]
                    user_history = User.objects.filter(email=username)[0]
                    history_obj = History(device=device_history, username=user_history, mode=usertype, status=status,
                                          dateandtime=time_now)

                    history_obj.save(force_insert=True)

                    if device_child == 1:

                        dev_obj = Device.objects.filter(device_id=device.device_id)[0]

                        dev_obj = {"req_status":"success","device_status": dev_obj.status, "device_id": dev_obj.device_id,
                                   "battery_status": dev_obj.battery_status, "op_status" : str(device_child)}

                        dev_obj = json.dumps(dev_obj)

                        return HttpResponse(dev_obj)

                    else:

                        return HttpResponse('{"req_status":"failed","message":"Status not updated"}')

                else:

                    return HttpResponse('{"req_status":"failed","message":"No user exsists"}')

            else:

                return HttpResponse(json.dumps({"req_status":"failed","message":"Error device not found with id " + device}))



        else:

            return HttpResponse('{"req_status":"failed","message":"Authentication Failed"}')



    except MultiValueDictKeyError:

        return HttpResponse('{"req_status":"failed","message":"Please provide required parameters for control.. See docs for more info"}')


def updateProfile(request):
    try:

        username = str(request.GET['user'])

        password = str(request.GET['pass'])

        phone = str(request.GET['phone'])

        address = str(request.GET['address'])

        dob = str(request.GET['dob'])

        if authenticate(username=username, password=password):

            number = User.objects.filter(email=username, password=password).update(phone=phone, address=address,

                                                                                   dob=dob)

            return HttpResponse('{"req_status":"success","update_status\"":' + str(number) + '\","message":"update successful"}')

        else:

            return HttpResponse('{"message":"Authentication Failed","req_status":"failed"}')

    except MultiValueDictKeyError:

        return HttpResponse('{"req_status":"failed","message":"Please provide required parameters for update profile.. See docs for more info"}')


def history(request):
    try:

        username = str(request.GET['user'])

        password = str(request.GET['pass'])

        device_ = str(request.GET['devid'])

        startdate = str(request.GET['startdate'])

        enddate = str(request.GET['enddate'])

        if authenticate(username=username, password=password):

            history_ = History.objects.filter(username=username, device_id=device_)

            transactions = []
            start_date_temp = startdate.split('/')
            start_date_temp[2] = start_date_temp[2][:4]
            start_date_obj = datetime.date(int(start_date_temp[2]), int(start_date_temp[1]), int(start_date_temp[0]))

            end_date_temp = enddate.split('/')
            end_date_temp[2] = end_date_temp[2][:4]
            end_date_obj = datetime.date(int(end_date_temp[2]), int(end_date_temp[1]), int(end_date_temp[0]))

            for h in history_:

                dev = Device.objects.filter(device_id=h.device.device_id)[0]
                dev_date = h.dateandtime.split('/')
                dev_date[2] = dev_date[2][:4]
                dev_date_obj = datetime.date(int(dev_date[2]), int(dev_date[1]), int(dev_date[0]))

                if (dev_date_obj > start_date_obj) and (dev_date_obj < end_date_obj):
                    map = {'device': h.device.device_id, 'devname': h.device.device_name, 'status': h.status,
                           'mode': h.mode, 'datetime': h.dateandtime, 'user': h.username.email,
                           'module_status': dev.module_status}
                    transactions.append(map)

            return HttpResponse(json.dumps({"req_status":"success","data":transactions}))

    except MultiValueDictKeyError:

        return HttpResponse('{"req_status":"failed","message":"Please provide the required parameter for showing the history"}')


# Login user and flood in data response to the user

def login(request):
    try:

        email = request.GET[Login.USER]

        password = request.GET[Login.PASS]

        userDetail = User.objects.filter(email=email, password=password)

        if userDetail.exists():

            userDetail = userDetail[0]

            user = {'name': userDetail.name, 'email': userDetail.email, 'dob': userDetail.dob,

                    'phone': userDetail.phone, 'address': userDetail.address}

            child_of_arr = []

            for c in json.loads(userDetail.child_of):
                child_of_arr.append(c)

            user['child_of'] = child_of_arr

            devices = Device.objects.filter(masteruser=userDetail)

            devices_list = []

            for d in devices:

                device_map = {"device_id": d.device_id, "device_name": d.device_name, "status": d.status,

                              "childusers": d.childuser, 'mode': d.mode, "macid":d.macid}

                children_arr = []

                for c in json.loads(d.childuser):
                    children_arr.append(c)

                device_map['childusers'] = children_arr

                devices_list.append(device_map)

            user["devices"] = devices_list

            #user = json.dumps(user, sort_keys=True)
            response = {"message":"Login successful","req_status":"success","data":user}
            return HttpResponse(json.dumps(response,sort_keys=True))

        else:

            return HttpResponse('{"req_status":"failed","message":"User is not registered"}')



    except MultiValueDictKeyError:

        return HttpResponse('{"req_status":"failed","message":"Please provide required parameters for login.. See docs for more info"}')



        # Register a User to the database


# 9337724384



def registerUser(request):
    try:

        name = request.GET[RegUser.NAME]

        dor = request.GET[RegUser.DOR]

        email = request.GET[RegUser.EMAIL]

        password = request.GET[RegUser.PASS]

        phone = request.GET[RegUser.PHONE]

        user = User(name=name, email=email, password=password, dor=dor, phone=phone)

        user.save(force_insert=True)

        return HttpResponse('{"req_status":"success","message":"registration successful"}')

    except MultiValueDictKeyError:

        return HttpResponse('{"req_status":"success","message":"Please provide required parameters for regmaster.. See docs for more info"}')

    except IntegrityError:

        return HttpResponse('{"req_status":"failed","message":"IntegrityError"}')


def getdevices(request):
    try:

        password = request.GET[RegUser.PASS]

        username = request.GET[RegUser.USER]

        if authenticate(username=username, password=password):

            device_map = {}

            devices_master = Device.objects.filter(masteruser=username)

            # Get the master devices

            devices_master_arr = []

            for d in devices_master:
                d_map = {'device_id': d.device_id, 'device_name': d.device_name, 'module_status': d.module_status,

                         'battery_status': d.battery_status, 'module_mode': d.module_mode}

                devices_master_arr.append(d_map)

            device_map['master_user'] = devices_master_arr

            # Get the devices where the user is a child

            child_of_arr = []

            user = User.objects.filter(email=username, password=password)[0]

            child_of_devices = json.loads(user.child_of)

            for c in child_of_devices:
                c_map = c

                c_devid = c['device_id']

                device_child_fetch = Device.objects.filter(device_id=c_devid)[0]

                c_map['module_status'] = device_child_fetch.module_status

                c_map['battery_status'] = device_child_fetch.battery_status

                c_map['mode'] = device_child_fetch.mode

                c_map['module_mode'] = device_child_fetch.module_mode

                child_of_arr.append(c_map)

            device_map['child_of'] = child_of_arr

            #outputObj = json.dumps(device_map)
            response = {"req_status":"success","data":device_map}
            return HttpResponse(json.dumps(response))

        else:

            return HttpResponse('{"req_status":"success","message":"Authentication failure"}')



    except MultiValueDictKeyError:

        # return HttpResponse('$failed$')

        return HttpResponse('{"req_status":"success","message":"Please provide required parameters for getdevices.. See docs for more info"}')


def removeUser(request):
    try:

        username = request.GET['user']

        password = request.GET['pass']

        device = request.GET['devid']

        child_user = request.GET['child']

        if authenticate(username=username, password=password):

            if is_user_exist(child_user):

                device_filtered = Device.objects.filter(device_id=device)[0]

                childuser = device_filtered.childuser

                childuser = json.loads(childuser)

                for child in childuser:

                    if child['user'] == child_user:
                        childuser.remove(child)

                        break

                childuser = json.dumps(childuser)

                user = User.objects.filter(email=child_user)[0]

                user = json.loads(user.child_of)

                for u in user:

                    if u['device_id'] == device:
                        user.remove(u)

                        break

                user = json.dumps(user)
                userchange = User.objects.filter(email=child_user).update(child_of=user)
                number = Device.objects.filter(device_id=device).update(childuser=childuser)
                response = {"req_status":"success","update_status_devices":str(number),"update_status_user":str(userchange),"message":"Removal of user " + child_user + " for device id " + device}
                return HttpResponse(json.dumps(response))

            else:

                return HttpResponse('{"req_status":"failed","message":"Child user doesn\'t exist"}')

        else:

            return HttpResponse('{"req_status":"failed","message":"Authentication failure"}')

    except MultiValueDictKeyError:

        return HttpResponse('{"req_status":"success","message":"Please provide all the required parameters for removing user.. See docs for more info"}')

    except ValueError:

        return HttpResponse('{"req_status":"failed","message":"Child user doesnot exist"}')


def addUser(request):
    try:

        username = request.GET['user']

        password = request.GET['pass']

        device = request.GET['devid']

        starttime = request.GET['starttime']

        endtime = request.GET['endtime']

        child_user = request.GET['child']

        if authenticate(username=username, password=password):

            if is_user_exist(child_user) and not (child_user_exist_for_device(deviceid=device, childUser=child_user)):

                device_filtered = Device.objects.filter(device_id=device, masteruser=username)[0]

                childOf_devices = User.objects.filter(email=child_user)[0].child_of

                childOf_devices = json.loads(childOf_devices)

                devchildObj = {'device_id': device, 'starttime': starttime, 'endtime': endtime,

                               'device_name': device_filtered.device_name}

                childOf_devices.append(devchildObj)

                childOf_devices = json.dumps(childOf_devices)

                userupdate = User.objects.filter(email=child_user).update(child_of=childOf_devices)

                childuser = device_filtered.childuser

                childuser = json.loads(childuser)

                childobj = {"user": child_user, "starttime": starttime, "endtime": endtime}

                childuser.append(childobj)

                childuser_json = json.dumps(childuser)

                number = Device.objects.filter(device_id=device).update(childuser=childuser_json)

                # return HttpResponse(
                #
                #     '{"req_status":"success","update_status_user":\"' + str(number) + '\","update_status_devices":\"' + str(
                #
                #         userupdate) + '\","message":"Added user ' + child_user + " for device id "+'\"}')
                response = {"req_status":"success","update_status_user":str(number),"update_status_devices":str(userupdate),"message":"Added user "+child_user}
                return HttpResponse(json.dumps(response))


            else:

                return HttpResponse('{"req_status":"failed","message":"Child user does not exist or child user for this device already exist"}')

        else:

            return HttpResponse('{"req_status":"failed","message":"Authentication failure"}')

    except MultiValueDictKeyError:

        return HttpResponse('{"req_status":"failed","message":"Please provide all the parameters"}')


def is_user_exist(childUser):
    user = User.objects.filter(email=childUser)

    if user.exists():
        return True
    else:

        return False


def child_user_exist_for_device(childUser, deviceid):
    device = Device.objects.filter(device_id=deviceid)

    if device.exists():

        device_child = device[0].childuser

        device_child = json.loads(device_child)

        flag = False

        for c in device_child:

            if c['user'] == childUser:

                flag = True

                break

            else:

                flag = False

        return flag

    else:

        return True

'''
    This function registers a new device by updating the exsisting Device status to
    OFF condition so that the device can be used with the Home Automation system
'''
def registerdevice(request):
    try:

        dev_id = str(request.GET[DeviceConst.DEVICE_ID])

        dev_name = request.GET[DeviceConst.DEVICE_NAME]

        dev_auth = str(request.GET[DeviceConst.AUTH_KEY])

        username = request.GET['user']

        password = request.GET['pass']

        number = 0

        if dev_auth == "epsumlabs_hashkey":

            userauth = User.objects.filter(email=username, password=password)

            if (userauth.exists()):

                device_filter = Device.objects.filter(device_id=dev_id, status="NEW")[0]
                number = Device.objects.filter(device_id=dev_id, status="NEW").update(
                    device_name=dev_name,
                    masteruser=userauth[0],
                    status="OFF")

                response = {"response":"success","message":"Device successfully added","device_id":dev_id,"rows":str(number),"macid":device_filter.macid}
                return HttpResponse(json.dumps(response))
            else:

                return HttpResponse('{"req_status":"failed","message":"User doesnot exist","macid":""}')



        else:

            return HttpResponse('{"req_status":"failed","message":"Cannot authenticate for device registration"}')

    except MultiValueDictKeyError:

        return HttpResponse('Please provide required parameters for regdevice.. See docs for more info ')

    except IntegrityError:

        return HttpResponse('Device with same id is already registered')

@csrf_exempt
def getStatusESP(request):
    try:

        data = str(request.GET['data'])
        data = data[1:]

        data = data.split('$')

        device_id = data[ESP.CIPHER]

        module_mode = str(data[ESP.MODULE_MODE])

        commode = data[ESP.COM_MODE]

        info = data[ESP.DATA]

        info_arr = info.split("B")
        battery = info_arr[1]
        op = info_arr[0]

        if 'FAILED' not in info:

            device = Device.objects.filter(device_id=device_id)

            if device.exists():

                dev_stat = Device.objects.filter(device_id=device_id)[0]
                stat = dev_stat.module_status
                device_update = Device.objects.filter(device_id=device_id).update(module_status=op,module_mode=commode,battery_status=battery)
                device_response = Device.objects.filter(device_id=device_id)[0]
                if stat != op :
                    #Get the details of the user to send push notifications
                    tokens=getTokens(dev_stat)
                    if op == 'D11' :
                        send_notification(tokens,"Device Locked","Device Activity")
                    elif op == 'D10' :
                        send_notification(tokens,"Device Unlocked","Device Activity")

                response = '$' + device_id + '$' + device_response.mode + '$' + module_mode + '$' + device_response.status+ '$'

                return HttpResponse(response)

            else:

                return HttpResponse('{"req_status":"failed","message":"Sorry your device isn\'t registered"}')
        else:
            Device.objects.filter(device_id=device_id).update(module_status='FAILED');
            dev_stat = Device.objects.filter(device_id=device_id)[0]
            tokens=getTokens(dev_stat)
            send_notification(tokens,"Failed","Activity")
            return HttpResponse('0')

    except MultiValueDictKeyError:

        return HttpResponse("failed")

    except IndexError:

        return HttpResponse("Error in ESP Format")

def getTokens(dev_stat):
    notify_tokens = []
    master_user = User.objects.filter(email=dev_stat.masteruser)[0]
    if master_user.notify_key != '' :
        notify_tokens.append(master_user.notify_key)
        ch_users = json.loads(dev_stat.childuser)
        for c in ch_users:
            ch_email=c['user']
            ch_key=User.objects.filter(email=ch_email)[0].notify_key
            if ch_key != '':
                notify_tokens.append(ch_key)
    return notify_tokens


@csrf_exempt
def passwordReset(request):
    try:
        user_mail = request.POST['user']
        token = request.POST['token']
        users = User.objects.filter(email=user_mail)
        if users.exists():
            User.objects.filter(email=user_mail).update(passresettoken=token)
            # email = EmailMessage('Password Reset',
            #                      'The OTP for the password reset is : ' + str(token),
            #                      'info@epsumlabs.com',
            #                      [user_mail],
            #                      reply_to=['info@epsumlabs.com']
            #                      )
            # mailoutput = email.send()
            server_response = send_mail(user_mail)
            response_ = json.dumps({"req_status":"success","message":"Mail successfully sent to "+str(user_mail),"status_response":server_response})
            return HttpResponse(response_)
        else:
            return HttpResponse('{"req_status":"failed","message":"Unable to send email"}')

    except BadHeaderError:
        return HttpResponse('{"req_status":"failed","message":"Invalid header found"}')
    except MultiValueDictKeyError:
        return HttpResponse('{"req_status":"failed","message":"Please provide all the parameters for password reset"}')


@csrf_exempt
def changePasswordAfterForgot(request):
    try:
        user = str(request.POST['user'])
        newpassword = str(request.POST['newpass'])
        authtoken = str(request.POST['token'])
        userObj = User.objects.filter(email=user, passresettoken=authtoken)
        if userObj.exists():
            update_status = User.objects.filter(email=user, passresettoken=authtoken).update(passresettoken='',
                                                                                             password=newpassword)
            return HttpResponse('{"req_status":"success","message":"Password changed","update_status":"' + str(update_status) + '"}')
        else:
            return HttpResponse('{"req_status":"failed","message":"Failed to authenticate. User doesn\'t exist"}')
    except MultiValueDictKeyError:
        return HttpResponse('{"req_status":"failed","message":"You\'ve not passed all parameters"}');


@csrf_exempt
def changePassword(request):
    try:
        user = str(request.POST['user'])
        newpassword = str(request.POST['newpass'])
        oldpassword = str(request.POST['oldpass'])
        user_filter = User.objects.filter(email=user, password=oldpassword)

        if (user_filter.exists()):
            user_filter = user_filter[0]
            affect = User.objects.filter(email=user, password=oldpassword).update(password=newpassword)
            return HttpResponse( '{"req_status":"success","update_status":\"'+ str(affect) +'\"}')
        else:
            return HttpResponse('{"req_status":"failed","message":"User doesn\'t exist"}')
    except MultiValueDictKeyError:
        return HttpResponse('{"req_status":"failed","message":"Please provide the required paramters"}')

import smtplib
def sendmail(request):
    msg = "From: {}\r\nTo: {}\r\n\r\n{}\r\n".format("support@epsumlabs.com", "er.krushnajena@gmail.com", "Test mail from epsumlabs support")
    try:
        user_mail = str(request.GET['email'])
        # email = EmailMessage('Test mail',
        #                      msg,
        #                      'info@epsumlabs.com',
        #                      [user_mail],
        #                      reply_to=['info@epsumlabs.com']
        #                      )
        # mailoutput = email.send()
        server = smtplib.SMTP('mail.epsumlabs.com',25)
        server.starttls()
        print(server.login("support@epsumlabs.com","Epsumlabs73778"))
        msg = "From: {}\r\nTo: {}\r\n\r\n{}\r\n".format("support@epsumlabs.com", "er.krushnajena@gmail.com", "Yo")
        response = server.sendmail("support@epsumlabs.com",[user_mail],msg)
        server.quit()
        responseObj = {"req_status":"Mail sent!!","serverResponse":response}
        return HttpResponse(json.dumps(responseObj))
    except BadHeaderError:
        return HttpResponse('Invalid header found.')
    except MultiValueDictKeyError:
        return HttpResponse("Please provide email parameter to send a mail")

def send_mail(user_mail):
    msg = "From: {}\r\nTo: {}\r\n\r\n{}\r\n".format("support@epsumlabs.com", "er.krushnajena@gmail.com", "Test mail from epsumlabs support")
    response = ""
    server = smtplib.SMTP('mail.epsumlabs.com',25)
    server.starttls()
    server.login("support@epsumlabs.com","Epsumlabs73778")
    msg = "From: {}\r\nTo: {}\r\n\r\n{}\r\n".format("support@epsumlabs.com", "er.krushnajena@gmail.com", "Yo")
    response = server.sendmail("support@epsumlabs.com",[user_mail],msg)
    server.quit()


        #responseObj = {"req_status":"Mail sent!!","serverResponse":response}
    return response



def notifyTest(request):
    try:
        message = str(request.GET['message'])
        token = str(request.GET['token'])
        title = str(request.GET['title'])

        return HttpResponse(send_notification(message=message,title=title,tokens=[token]))
    except(MultiValueDictKeyError):
        return HttpResponse("Please provide the required parameters : message,token and title")


@csrf_exempt
def register_token(request):
    try:

        token = str(request.POST['notify_token'])
        user_name = str(request.POST['user'])
        password = str(request.POST['pass'])
        user_filter = User.objects.filter(email=user_name, password=password)
        if (user_filter.exists()):
            filter = User.objects.filter(email=user_name, password=password).update(notify_key=token)
            return HttpResponse(
                '{"req_status":"success","message":"token registration successful","rows":"' + str(filter) + '"}')
        else:
            return HttpResponse('{"req_status":"failed","message":"user not found"}')

    except MultiValueDictKeyError:
        return HttpResponse('{"req_status":"failed","message":"required parameters are not found"}')


class ESP:
    CIPHER = 0

    COM_MODE = 1

    MODULE_MODE = 2

    DATA = 3

    BATTERY = 4

    COM_MODE_ONLINE = 1

    COM_MODE_OFFLINE = 0

    MODULE_MOD_HARDREST = 1

    MODULE_MOD_DATA = 2

    MODULE_MOD_EEPROM = 3
